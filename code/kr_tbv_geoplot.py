#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May  2 11:21:10 2018

@author: kooskoets

Doet de pre-processing van het totale meetwaardenbestand van RWS.
"""

import pandas as pd

from bokeh.io import show
from bokeh.models import ColumnDataSource, GMapOptions, HoverTool
from bokeh.plotting import gmap
import os

#  Juiste bestand inlezen
df = pd.read_csv('data/kr_alle_stations_breed_kwc.csv')
# Rijen zonder chlorofylmetingen verwijderen
df.dropna(subset=['CHLFa_NVT'], inplace=True)
# Datumkolom converteren naar 'echt' datumformaat
df['DATUM'] = pd.to_datetime(df['DATUM'])
#Maand en jaar bepalen, wil zo te zien alleen op deze manier
df['month'] = df.apply(lambda row: row.DATUM.month, axis=1)
df['year'] = df.apply(lambda row: row.DATUM.year, axis=1)

#Kies jaar 2014, min of meer arbitrair
df_2014 = df.query('year == "2014"')
df_2014.to_csv('tussenresultaten/csv/Alle Stations/tbv geoplots/jaar2014.csv')

#Dit doen om aantal overblijvende stations uit te vragen
#df_2014[['LOC']].drop_duplicates().shape

#Alleen zomermaanden selecteren, gedefinieerd als juli, augustus en september
#Dat zijn de maanden waarin de overlast t.g.v. algen normaal het ergst is.
df_summer = df_2014.query('month in [7, 8, 9]')

#Groepeer per meetstation, reset de index
df_mean = df_summer.groupby(by='LOC')[['CHLFa_NVT', 'P_NVT']].mean().reset_index()
#Bewaren voor later
#df_mean.to_csv('gemiddeld chl en fosf.csv')

df_stations = pd.read_csv('tussenresultaten/csv/Alle Stations/kr_meetstations met X_WGS en Y_WGS.csv', sep=';')

df_plot = pd.merge(df_stations, df_mean, on='LOC')

dct_stat_coord_chl = dict(y=df_plot['Y_WGS'].tolist(), x=df_plot['X_WGS'].tolist(), radius=df_plot['CHLFa_NVT']/2, ID=df_plot['LOC'].tolist(), 
                      desc=df_plot['LOCOMS'].tolist())

#Set the options for the map that shows up initially
map_options = GMapOptions(lat=52.3, lng=5.5, map_type="roadmap", zoom=8)

#When hovering over a station, the ID and description should be displayed
hover = HoverTool(tooltips=[("ID:", "@ID"), ("Oms:", "@desc")])    

#The first argument of gmap is the API key from Google
plot = gmap("AIzaSyCqw0nnYlcAIjTV70Jd53d630iSdY4uKEE", map_options, title="Zomergemiddelden chlorofyl (groen) en fosfor (rood), 2014", 
            plot_width=1000, plot_height=800)
plot.add_tools(hover)


source = ColumnDataSource(
    data=dict(lat=df_plot['Y_WGS'].tolist(),
              lon=df_plot['X_WGS'].tolist(),
              rad1=df_plot['CHLFa_NVT']*1.3,
              rad2=df_plot['P_NVT']*80,
              ID=df_plot['LOC'].tolist(),
              desc=df_plot['LOCOMS'].tolist())
    )

plot.wedge(x="lon", y="lat", radius="rad1", radius_units="screen", start_angle=1.57,
           end_angle=4.71, color="green", fill_alpha=0.8, source=source)

plot.wedge(x="lon", y="lat", radius="rad2", radius_units="screen", start_angle=4.71,
           end_angle=1.57, color="red", fill_alpha=0.8, source=source)

show(plot)