#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 13 11:58:59 2018

@author: kooskoets
"""

import pandas as pd
from bokeh.plotting import figure, output_file, show

locatie = "SCHAARVODDL"

df_st_breed = pd.read_csv('data/kr_alle_stations_breed.csv')
df_meetstations = pd.read_csv('data/meetstations.csv')

df = df_st_breed.query("LOC == '"+locatie+"'")
loc_oms = df_meetstations.query("LOC == '"+locatie+"'")['LOCOMS'].values[0]

df['DATUM'] = pd.to_datetime(df['DATUM'])

p = figure(plot_width=1000, plot_height=650, x_axis_type="datetime")
p.title.text = 'Temperatuur, chlorofyl en nutriënten op meetstation ' + loc_oms

for subst, name, scalef, color in zip(['T_NVT', 'CHLFa_NVT', 'NO2_Nnf', 'NO3_Nnf', 'P_NVT', 'SiO2_Sinf'], 
                                      ["Temperatuur", "Chlorofyl A", "NO2", "NO3", "Tot. fosfaat", "Silicaat"], 
                                      [5, 2, 1250, 20, 250, 10], ['red', 'green', 'aqua', 'blue', 'purple', 'grey']):
    p.line(df['DATUM'], scalef*df[subst], line_width=2, color=color, alpha=0.8, legend=name)

p.legend.location = "top_left"
p.legend.click_policy="hide"""
p.legend.orientation = "horizontal"

output = "tussenresultaten/interactieve_html/" + loc_oms + ".html"
output_file(output, title=loc_oms + ", temperatuur, nutriënten en chlorofyl")

show(p) 
