#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on May 15 


@author: kooskoets

N.B. to launch, execute 'bokeh serve --show --port 5002 plot_stations_bokeh.py'.
The port number is arbitrary, but if a port conflict arises, just choose another number, e.g. 5003, 5004 etc.
"""

import pandas as pd
import numpy as np
import datetime

from bokeh.layouts import column
from bokeh.models import GMapOptions, HoverTool
from bokeh.plotting import gmap, curdoc, Figure
import os

os.chdir("/Users/kooskoets/Documents/ITVitae/Cursussen/Data science/Projecten/RWS-loc")

#Lees de lijst van meetstations met geografische lengte en breedtegraad
df_stations = pd.read_csv('data/kr_meetstations met X_WGS en Y_WGS.csv', sep=';')

dct_stat_coord = dict(y=df_stations['Y_WGS'].tolist(), x=df_stations['X_WGS'].tolist(), ID=df_stations['LOC'].tolist(), 
                      desc=df_stations['LOCOMS'].tolist())
#Set the data source for Bokeh to the dictionary created above, containing the appropriate data
    
#Set the options for the map that shows up initially
map_options = GMapOptions(lat=52.3, lng=4.5, map_type="roadmap", zoom=7)

hover = HoverTool(tooltips=[("ID:", "@ID"), ("Oms:", "@desc")])    

#The first argument of gmap is the API key from Google
#plot = gmap("AIzaSyCqw0nnYlcAIjTV70Jd53d630iSdY4uKEE", map_options, title="Kwaliteitsmeetnet Rijkswaterstaat")
plot = gmap("AIzaSyCqw0nnYlcAIjTV70Jd53d630iSdY4uKEE", map_options, title="Kwaliteitsmeetnet Rijkswaterstaat", 
            plot_width=1000)
plot.add_tools(hover)

r = plot.circle(x=[], y=[], size=5, color="red", fill_alpha=0.8)
#bokeh.plotting.curplot().plot_width=800

#src = ColumnDataSource(acft_pos)
ds=r.data_source
ds.data = dct_stat_coord

# put the button and plot in a layout and add to the document
curdoc().add_root(column(plot))
