#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May  2 11:21:10 2018

@author: kooskoets
"""

import pandas as pd
import matplotlib.pyplot as plt
import geopandas as gpd
from shapely.geometry import Point
import mplleaflet

# Poging csv in stukken van 100,000 rijen te lezen om RAM niet te overbelasten, 
# na_values: extreme uibijters chlfa naar NaN
chunksize = 100000
TextFileReader = pd.read_csv('../data/data_tot.csv', encoding="latin1", index_col=0, 
                             chunksize=chunksize, na_values=[1000, -1000], iterator=True)
df = pd.concat(TextFileReader, ignore_index=True)

#Selecteer de kolommen die betrekking hebben op de meetstations en hun locaties
df_loc = df[['LOC', 'LOCOMS', 'X_RD', 'Y_RD']]

#Verwijder de duplicaten. De waarschuwing kan worden genegeerd, het is ook maar een caveat ;-)
df_loc.drop_duplicates(inplace=True)
#Je houdt een lijst van 114 meetstations over

#Schrijf lijst meetstations naar schijf
df_loc.to_csv('meetstations.csv')

#Onderstaande regel incommenteren als je niet het hele dataframe in wilt laden
#df_loc = pd.read_csv('meetstations.csv')

#Converteer losse Rijksdriehoekscoördinaten X en Y naar Point-objecten 
location = [Point(xy) for xy in zip(df_loc.X_RD, df_loc.Y_RD)]
df_loc.drop(['X_RD', 'Y_RD'], axis=1, inplace=True)

#Stel het coördinatensysteem in op Rijksdriehoeks
crs = {'init': 'epsg:28992'}

#Maak assen in MatPlotLib
fig, ax = plt.subplots(figsize=(10,10), subplot_kw={'aspect':'equal'})

#Converteer naar dataframe
geo_rws = gpd.GeoDataFrame(df_loc , crs=crs, geometry=location)

#Plot op kaart
mplleaflet.show(fig=geo_rws.plot(color='red', ax=ax).figure, crs={'init': 'epsg:28992'})
